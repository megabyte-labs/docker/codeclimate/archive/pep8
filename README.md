# pep8 - Python style guide checker

pep8 is a tool to check your Python code against some of the style
conventions in [PEP 8][pep8].

# codeclimate-pep8

For information on using pep8 with [Code Climate][cc], see the
[Code Climate pep8 docs][docs].

## configuration

The PEP8 project has been renamed to pycodestyle. The Code Climate PEP8 engine
has not been updated to incorporate that name change.

To make configuration specifications in your `setup.cfg` file or `tox.ini` file,
the Code Climate PEP8 engine requires that you include those changes in:

* a `[pep8]` config section
* instead of `[pycodestyle]` config section.

[pep8]: http://www.python.org/dev/peps/pep-0008/
[cc]: https://codeclimate.com/
[docs]: https://docs.codeclimate.com/docs/pep8


[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#example-usage)

## ➤ Requirements

- **[Docker](https://gitlab.com/megabyte-labs/ansible-roles/docker)**
- [CodeClimate CLI](https://github.com/codeclimate/codeclimate)

### Optional Requirements

- [DockerSlim](https://gitlab.com/megabyte-labs/ansible-roles/dockerslim) - Used for generating compact, secure images
- [Google's Container structure test](https://github.com/GoogleContainerTools/container-structure-test) - For testing the Docker images




[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#example-usage)


## ➤ Example Usage

The Code Climate engine built using this repository can be used for analysis using code climate cli. 

```shell
codeclimate analyze
```

This allows you to run code climate analysis from the root of  your project directory. Ensure `.codeclimate.yml` file is present on the root of your project directory. A sample configuration of this file  is present in this repository.Once the analysis is complete it will display the results in code climate format.

If you have created the docker image locally and wish to test it you can do so using below command

```shell
codeclimate analyze --dev
```
In order to run slim docker image of this engine , please pull the latest slim docker image locally ( or create one ) and retag it to latest before running the same.



### Building the Docker Container

Run the below make command from the root of this repository to create a local fat docker image
```shell
make image
```

### Building a Slim Container

Run the below make command from the root of this repository to create a local slim docker image
```shell
make slim
```


### Test

Run the below command from the root of this repository to test the images created by this repository.
```shell
make test
```
