.PHONY: image

IMAGE_NAME ?= codeclimate/codeclimate-pep8

SLIM_IMAGE_NAME ?= codeclimate/codeclimate-pep8:slim

image:
	docker build --rm -t $(IMAGE_NAME) .

slim: image
	docker-slim build --tag $(SLIM_IMAGE_NAME) --http-probe=false --exec '/usr/src/app/codeclimate-pep8' --mount "$$PWD/tests:/code" --workdir '/code' --preserve-path '/usr/lib/python3.9/site-packages/pycodestyle' --preserve-path '/usr/src/app/tests/' $(IMAGE_NAME) && prettier --write slim.report.json 

test: slim
	container-structure-test test --image $(IMAGE_NAME) --config tests/container-test-config.yaml && container-structure-test test --image $(SLIM_IMAGE_NAME) --config tests/container-test-config.yaml

